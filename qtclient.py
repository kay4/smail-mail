import sys
import smtplib

import settings

from imaplib import IMAP4_SSL
from PyQt5.QtWidgets import (QWidget, QGridLayout, QFormLayout, QPushButton, QApplication,
                             QTextEdit, QLabel, QLineEdit)


class EmailClient(QWidget):
    def __init__(self, title='Smail Mail', dimensions=(1024, 768)):
        super().__init__()
        self.title = self.setWindowTitle(title)
        self.size = self.setGeometry(500, 100, *dimensions)
        self.controls = ['Write', 'Send', 'Reply', 'Forward']
        self.header = ['To', 'From', 'Subject']
        self.recipient = QLineEdit()
        self.sender = QLineEdit()
        self.subject = QLineEdit()
        self.message = QTextEdit()
        self.submit = QPushButton('Send')

        self.init_form()

    def init_form(self):
        form = QFormLayout()
        form.addRow(QLabel('From'), self.sender)
        form.addRow(QLabel('To'), self.recipient)
        form.addRow(QLabel('Subject'), self.subject)
        form.addRow(QLabel('Message'), self.message)
        form.addRow(QLabel('Send'), self.submit)
        self.submit.clicked.connect(self.send_email)
        self.setLayout(form)

        self.show()

    def send_email(self):
        print("Sending email ...")
        self.sender.text()
        server = smtplib.SMTP_SSL(settings.SMTP_SERVER, 465)
        server.login(settings.EMAIL_USER, settings.EMAIL_PASSWORD)
        server.sendmail(self.sender.text(), self.recipient.text(), self.message.toPlainText())
        print("Sent.")

    def get_inbox(self):
        connection = IMAP4_SSL(settings.IMAP_SERVER)
        connection.login(settings.EMAIL_USER, settings.EMAIL_PASSWORD)

    def create_layout(self, rows=3, cols=1, start=0):
        return [(row, col) for row in range(start, rows) for col in range(cols)]


if __name__ == '__main__':
    app = QApplication(sys.argv)

    client = EmailClient()

    sys.exit(app.exec_())
