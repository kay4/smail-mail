import smtplib

from tkinter import *
from email.message import EmailMessage
from email.policy import SMTP


class EmailFrame(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.recipient = StringVar()
        self.sender = StringVar()
        self.subject = StringVar()
        self.message = StringVar()
        self.create_entries()
        self.create_form()

    def create_entries(self):
        recipient = Entry(self.master, textvariable=self.recipient).grid(row=0, column=1)
        sender = Entry(self.master, textvariable=self.sender).grid(row=1, column=1)
        subject = Entry(self.master, textvariable=self.subject).grid(row=2, column=1)
        message = Text(self.master, width=100, height=10).grid(row=3, column=1)

    def create_form(self):
        Label(self.master, text='To:').grid(row=0)
        Label(self.master, text='From:').grid(row=1)
        Label(self.master, text='Subject:').grid(row=2)
        Label(self.master, text='Message:').grid(row=3)

        self.submit = Button(self.master, text='Send', command=self.send).grid(row=4)

    def send(self, *args):
        print("Sending email message ...")
        msg = EmailMessage(policy=SMTP)
        msg['To'] = self.recipient.get()
        msg['From'] = self.sender.get()
        msg['Subject'] = self.subject.get()
        msg.set_content(self.message.get())

        with smtplib.SMTP() as server:
            server.ehlo()
            server.starttls()
            server.login()
            server.send_message(msg)

    def close(self):
        self.quit = Button(self.master, text="QUIT", command=self.master.destroy).grid(row=5)


root = Tk()
app = EmailFrame(master=root)
app.master.title('Smail Mail')
app.master.geometry('800x600')
app.mainloop()
